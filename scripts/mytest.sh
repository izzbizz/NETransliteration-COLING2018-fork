#!/usr/bin/env bash

# Copyright 2018 Amazon.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to 
# deal in the Software without restriction, including without limitation the 
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
# sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
# THE SOFTWARE.

set -e

if [ $# -ne 2 ]
then
    echo "Usage: $0 testInput modelDir name"
    echo " testInput: name of dataset"
    exit 1
fi

testInput="../../mame-loshn/data/$1"
modelDir="../models/$2"

paste <(cut -f1 "${testInput}") > "${testInput}.words"
# add the root of the repo to the python path
export PYTHONPATH=$(readlink -f ../):$PYTHONPATH
echo "Scoring with tensor2tensor"
echo $PYTHONPATH
python "../xlit_t2t/app.py" \
	--decode "$testInput" \
	--return_beams \
	--beam_size 3 \
	--model_dir "${modelDir}" \
	--output "${testInput}.decoded"

./score.py "$testInput" "${testInput}.decoded" "${testInput}.words"
