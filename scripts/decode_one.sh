#!/usr/bin/env bash

set -e

if [ $# -ne 2 ]
then
    echo "Usage: $0 testInput modelDir method"
    echo " testInput: the input test file (the wd_dataset_20 file, not .words)"
    exit 1
fi

testInput=$1
modelDir="../models/$2"

echo "${testInput}" > "${testInput}.words"
# add the root of the repo to the python path
export PYTHONPATH=$(readlink -f ../):$PYTHONPATH
# run the appropriate scoring
echo "Scoring with tensor2tensor"
echo $PYTHONPATH
python "../xlit_t2t/app.py" \
	--decode "${testInput}.words" \
	--return_beams \
	--beam_size 3 \
	--model_dir "${modelDir}" \
	--output "${testInput}.decoded"

cat "${testInput}.decoded"
rm "${testInput}.words"
rm "${testInput}.decoded"
