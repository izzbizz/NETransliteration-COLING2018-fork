#!/usr/bin/env bash

# Copyright 2018 Amazon.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to 
# deal in the Software without restriction, including without limitation the 
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
# sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
# THE SOFTWARE.

set -e

if [ $# -ne 3 ]
then
    echo "Usage: name of model directory in ../models/"
    echo " inputPrefix: name of dataset in ../data/"
    echo " dropout: recommend 0.4 for LC, 0.2 for YIVO"    
exit 1
fi

inputPrefix="../../mame-loshn/data/$1"
modelDir="../models/$2"
dropout=$3
num_epochs=6
num_units=64
num_heads=4
numLayers=2
mkdir -p $modelDir

export PYTHONPATH=$(readlink -f ../):$PYTHONPATH
# run TF training
echo "Training with tensor2tensor"
paste <(cut -f1,2 "${inputPrefix}_64") > "${inputPrefix}_64.f12"
paste <(cut -f1,2 "${inputPrefix}_16") > "${inputPrefix}_16.f12"
echo $PYTHONPATH
python "../xlit_t2t/app.py" \
	--train "${inputPrefix}_64.f12" \
	--valid "${inputPrefix}_16.f12" \
	--model_dir "${modelDir}" \
	--size $num_units \
	--max_epochs $num_epochs \
	--num_layers $numLayers \
	--num_heads $num_heads \
	--dropout $dropout

