# NETransliteration-COLING2018-fork

This is a fork of Yuval Merhav's and Stephen Ash's repository https://github.com/steveash/NETransliteration-COLING2018. It serves to replicate the results for my Master's thesis on neural back-transliteration for Yiddish (the main repository is https://codeberg.org/izzbizz/mame-loshn).

The repo is setup with folders:

- `scripts` - Merhav & Ash's bash scripts and python scripts to prepare data, run training, and testing. I minimally adapted the scripts for data-splitting, training and inference, saving them as `myprepare.sh`, `mytrain.sh` and `mytest.sh`
- `models` - my pretrained models for library (_LC_) and YIVO data
- `xlit_t2t` - Merhav & Ash's adaptation of Tensor2Tensor to work for the task of named entity transliteration

You can run testing on the entire heldout validation dataset, or just on single words using the `decode_one.sh` script. You could also retrain the models, but that is probably not necessary, given that there are already pretrained models in the `models` folder.

# Replicating Tensorflow experimental results
## First time setup
To replicate the results or for inference:
1. set up a virtual environment with Python 3.6 and pip install the requirements.txt:
```bash
python3 -m pip install --upgrade pip
python3 -m pip install --user virtualenv
python3 -m virtualenv --python=3.6 env36
```
2. Activate the virtual environment and install the required packages:
```bash
source env36/bin/activate
pip install -r path/to/requirements.txt
```
## Training and testing
1. Activate the virtual env
2. `cd ~/NETransliteration-COLING2018-fork/scripts`
3. *_This step is optional_* Run training for either YIVO or library data using `mytrain.sh` 
    - The first argument is the name of the input data (contained in `../../mame-loshn/data`) file _without_ the slice. 
    - The second argument is the name of the the model. The model will be written into the `../models` folder.
    - The third argument is the dropout rate. The best results were achieved with a dropout of 0.4 for the library data and 0.2 for YIVO.
```bash
./mytrain.sh yivo_small yivo 0.2
./mytrain.sh lc lc 0.4
```
4. Run testing for the held out validation set on the pretrained models (or alternatively on the models trained in the previous step).
    - Pass the entire name of the test data file but without the path as the first argument, and the model name as the second:`
```bash
./mytest.sh yivo_small_20_no_dup yivo
./mytest.sh lc_20_no_dup lc
```

This produces result summary with the 1best, 2best, 3best accuracy like:
```bash
total tested: 5468
matches:
 1best: 4957 (95.99%)
 2best: 157 (3.04%)
 3best: 50 (0.97%)
accuracy:
 1best: 0.91
 2best: 0.94
 3best: 0.94
```

Matches 1best, 2best, etc. is the count of words correctly predicted that appeared in this position in the top-k results from the decoder. The percentages in the parenthesis indicate the % of total tested words that appeared in that spot.

Accuracy is the % of words that appeared anywhere in the top-k results. Thus the 2best score includes correct words predicted that showed up in either the top spot or second spot in the results. The accuracy here is 1.0 - WER (Word Error Rate).

5. Finally, you can also let the model decode a single word using the `decode_one` script. Pass the string to decode as the first and the model name as the second argument:

```bash
decode_one.sh mame-loshn yivo
```
# Re-creating the data

The datasets reside in the `mame-loshn` repository. They have already been split into training (64 \%), dev (16 \%) and test (20 \%) data using a modified version of Merhav & Ash's `prepare_input.sh` script. If you want to split the data again, run the `myprepare.sh` script:

```bash
cd ~/NETransliteration-COLING2018-fork/scripts
./myprepare.sh ../../mame-loshn/data/yivo_small
```

Note that the data may contain the same source string several times (as long as the targets are different). To allow for a fair comparison with the baselines systems, the duplicates have to be removed from the test files (containing the `_20` suffix).

# Licenses
The code in `xlit_s2s_nmt` and `xlit_t2t` are adapted from other tensorflow repositories and is licensed under the original Apache 2 licenses.

The `scripts` folder contains data preparation and train/test scripts licensed under the MIT License.